import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class WarehouseSearchSystem {
    private List<Product> inventory;

    public WarehouseSearchSystem() {
        inventory = new ArrayList<>();
    }

    public void loadInventory(String filePath) {
        try (BufferedReader reader = new BufferedReader(new FileReader(filePath))) {
            String line;
            while ((line = reader.readLine()) != null) {
                Product product = parseProduct(line);
                inventory.add(product);
            }
        } catch (IOException e) {
            System.out.println("Error while reading inventory file: " + e.getMessage());
        }
    }

    public void searchProduct(String parameter, String value) {
        List<Product> searchResults = new ArrayList<>();
        for (Product product : inventory) {
            if (product.matchesParameter(parameter, value)) {
                searchResults.add(product);
            }
        }

        if (searchResults.isEmpty()) {
            System.out.println("Products not found.");
        } else {
            searchResults.forEach(System.out::println);
        }
    }

    public void viewAllProducts() {
        inventory.forEach(System.out::println);
    }

    public void sortProductsByParameter(String parameter) {
        Comparator<Product> comparator;
        switch (parameter) {
            case "id":
                comparator = Comparator.comparing(Product::getId);
                break;
            case "name":
                comparator = Comparator.comparing(Product::getName);
                break;
            case "category":
                comparator = Comparator.comparing(Product::getCategory);
                break;
            default:
                System.out.println("Invalid sorting parameter.");
                return;
        }

        inventory.sort(comparator);
        System.out.println("List of products sorted by parameter: " + parameter);
        inventory.forEach(System.out::println);
    }

    public static void main(String[] args) {
        WarehouseSearchSystem app = new WarehouseSearchSystem();
        app.loadInventory("C:/Users/user/IdeaProjects/BahodirjonRahmatullev_FurnitureWarehouse/src/inventory.txt");

        Scanner scanner = new Scanner(System.in);
        boolean running = true;

        System.out.println("Welcome to the Warehouse Search System!");

        while (running) {
            System.out.println("Enter a command (search, view, sort, exit):");
            String command = scanner.nextLine();

            switch (command.toLowerCase()) {
                case "search":
                    System.out.println("Enter the search parameter:");
                    String parameter = scanner.nextLine();
                    System.out.println("Enter the value:");
                    String value = scanner.nextLine();
                    app.searchProduct(parameter, value);
                    break;
                case "view":
                    app.viewAllProducts();
                    break;
                case "sort":
                    System.out.println("Enter the sorting parameter (id, name, category):");
                    String sortParameter = scanner.nextLine();
                    app.sortProductsByParameter(sortParameter);
                    break;
                case "exit":
                    running = false;
                    break;
                default:
                    System.out.println("Invalid command.");
                    break;
            }
        }

        System.out.println("Application finished.");
    }

    private static Product parseProduct(String line) {
        String[] parts = line.split(",");
        String id = parts[0].trim();
        String name = parts[1].trim();
        String category = parts[2].trim();
        double price = Double.parseDouble(parts[3].trim());
        int quantity = Integer.parseInt(parts[4].trim());
        return new Product(id, name, category, price, quantity);
    }
}

class Product {
    private String id;
    private String name;
    private String category;
    private double price;
    private int quantity;

    Product(String id, String name, String category, double price, int quantity) {
        this.id = id;
        this.name = name;
        this.category = category;
        this.price = price;
        this.quantity = quantity;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getCategory() {
        return category;
    }

    public boolean matchesParameter(String parameter, String value) {
        switch (parameter) {
            case "id":
                return id.equalsIgnoreCase(value);
            case "name":
                return name.equalsIgnoreCase(value);
            case "category":
                return category.equalsIgnoreCase(value);
            default:
                return false;
        }
    }

    @Override
    public String toString() {
        return "Product{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", category='" + category + '\'' +
                ", price=" + price +
                ", quantity=" + quantity +
                '}';
    }
}
